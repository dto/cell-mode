#+TITLE: Cell-mode

Cell-mode is an object-oriented spreadsheet control for GNU Emacs. It
provides a major mode for spreadsheet-based user interfaces; it can be
further extended by defining application-specific Emacs Lisp minor
modes which supply new cell and spreadsheet classes via Emacs'
included object system, EIEIO.

* Video demo

 - https://emacsconf.org/2019/talks/18/

* Features

 - Uses a simple file format based on EIEIO-PERSISTENT.  Cell-mode
   files can be visited, saved, loaded, and so on just like any other
   text file.  Works with Emacs' existing autosave, version control,
   TRAMP, and backup features.
 - You can cut, copy, and paste cells between sheets using typical
   Emacs shortcuts, as well as insert and delete rows and
   columns. (CUA-mode not yet supported.)
 - Undo/redo support.
 - Execute Lisp expressions in cells, and other user defined actions.
 - Rectangle selection via keyboard and mouse-drag are available.
 - Object-orientation through the use of Emacs' included object
   system, EIEIO.  Cells can contain any Lisp value and respond
   polymorphically to events.
 - Contents can be collected and processed however you want.
 - Cells can display images, Unicode text, and variable-width fonts. 

* Issues

 - Cursor can display incorrectly in cells with images and some
   Unicode characters.
 - No data dependency resolution, but this will be implemented.
 - Needs more optimization.
 - Columns are sometimes misaligned when changing font sizes in a
   frame; restart Cell-mode with "M-x cell-mode RET" to restore proper
   formatting.

* Applications

 - Check out [[https://gitlab.com/dto/mosaic][Mosaic]] for examples of using Cell-mode in an application.
